# Why & what
The goal of the project is to build a hub allowing multiple non-profit organizations (NPOs) to centralize their demand, inventory and case management to move inventory from helping countries and deliver help where it's needed.

See `docs` folder for documentation/drawings
See `proto` folder for an attempt to implement a basic inventory/demand API that would allow systems to plug in. The current idea is to implement a REST API backend and build the UI with rapid prototyping tools/applications builders, such as `unqork`, `budibase` and others.

# Help wanted
We need help from the engineering community to prototype and launch this quickly:

- Python engineers, familar with django and DRF
- Folks familiar with no/low code engineering tools to build UI
- Analysts to help structure the models for the inventory and demand management
- Folks who can discover and adapt a case management system to integrate with it

`abolotnov@gmail.com` - feel free to email if you have thoughts/suggestions.
