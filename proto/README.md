Simple `django` app with an intent to serve as the backend for the demands and inventory management system. 

# Run

I am keeping the database checked-in so that you don't have to migrate and push test data into the backend (there isn't much but better than nothing). To install/run:

- Install Python 3.8+
- `pip install -r requirements.txt` (starting in a new virtual environment is recommended)
- `cd be`
- `python manage.py runserver`

This should start it and going to `http://localhost:8000/api` should take you to the api root.