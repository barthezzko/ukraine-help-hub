"""be URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from inventory import views
from rest_framework import routers
from inventory.api import views as api_views

rest_router = routers.DefaultRouter()
rest_router.register('org_application', api_views.OrgRegistrationApplicationViewSet, basename='org_application')
rest_router.register('org', api_views.OrgViewSet, basename='org')
rest_router.register('warehouse', api_views.WarehouseViewSet, basename='warehouse')
rest_router.register('address', api_views.AddressViewSet, basename='address')
rest_router.register('item', api_views.ItemViewSet, basename='item')
rest_router.register('category', api_views.CategoryViewSet, basename='category')
rest_router.register('subcategory', api_views.SubCategoryViewSet, basename='subcategory')
rest_router.register('tag', api_views.TagViewSet, basename='tag')
rest_router.register('available_item', api_views.ItemAvailabilityViewSet, basename='available_item')
rest_router.register('demand', api_views.DemandViewSet, basename='demand')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('status/', views.status),
    path('api/', include(rest_router.urls))

]
