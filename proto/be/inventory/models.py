from django.core.validators import RegexValidator
from django.db import models
from address.models import AddressField
from phone_field.models import PhoneField
from django.contrib.auth.models import User
from django.db.models import Sum


class DayOfTheWeek(models.TextChoices):
    Monday = 'M'
    Tuesday = 'T'
    Wednesday = 'W'
    Thursday = 'Th'
    Friday = 'F'
    Saturday = 'Sat'
    Sunday = 'S'


class OrgRegistrationStatus(models.TextChoices):
    PENDING_APPROVAL = 'pending',
    APPROVED = 'approved',
    DECLINED = 'declined'


class QuantityMeasure(models.TextChoices):
    Kilogram = 'kg',
    Milliliter = 'ml',
    Box = 'box',
    SingleItem = 'item',
    Other = 'other',
    Bottle = 'bottle'


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    org = models.OneToOneField('Org', on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False)


class Org(models.Model):
    name = models.CharField(max_length=128, unique=True, blank=False, null=False)
    web = models.URLField(blank=True, null=True)
    phone = PhoneField(blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    active = models.BooleanField(default=False)
    screened = models.BooleanField(default=False)
    managers = models.ManyToManyField(User)

    def __str__(self):
        return self.name


class OrgRegistrationApplication(models.Model):
    name = models.CharField(max_length=256, blank=False, null=False)
    web = models.URLField(max_length=256, blank=True, null=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                    message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone = models.CharField(max_length=15, blank=False, null=False, validators=[phone_regex])
    email = models.EmailField(null=False, blank=False)
    date_submitted = models.DateTimeField(auto_now_add=True)
    admin_first_name = models.CharField(max_length=128, null=False, blank=False, default='unknown')
    admin_last_name = models.CharField(max_length=128, null=True, blank=True, default='unknown')
    admin_email = models.EmailField(null=False, blank=False)
    admin_phone = models.CharField(max_length=15, null=False, blank=False, validators=[phone_regex])
    application_status = models.CharField(max_length=22, null=False, blank=False, choices=OrgRegistrationStatus.choices, default=OrgRegistrationStatus.PENDING_APPROVAL)
    notes = models.TextField(null=True, blank=True)
    review_notes = models.TextField(null=True, blank=True)


class OperatingHours(models.Model):
    day = models.TextField(choices=DayOfTheWeek.choices, null=False, blank=False)
    start = models.TimeField(null=False, blank=False)
    end = models.TimeField(null=False, blank=False)
    site = models.ForeignKey('Warehouse', on_delete=models.RESTRICT, null=False, blank=False)

    def __str__(self):
        return f"{self.day}: {self.start} - {self.end}"


class Warehouse(models.Model):
    name = models.CharField(max_length=64, blank=False, null=False, unique=True)
    org = models.ForeignKey(Org, on_delete=models.RESTRICT)
    address = AddressField(blank=True, null=True)  # disabled address for now - see django-address module tests on how to work with it
    phone = PhoneField(blank=True, null=True)
    email = models.EmailField(blank=True, null=True)

    def __str__(self):
        return f"{self.name} at {self.address}"


class Tag(models.Model):
    name = models.CharField(max_length=64, null=False, blank=False, unique=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.lower()
        super().save(*args, **kwargs)


class Category(models.Model):
    name = models.CharField(max_length=256, null=False, blank=False, unique=True)

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=False, blank=False)
    name = models.CharField(max_length=256, null=False, blank=False)

    class Meta:
        unique_together = ['category', 'name']

    def __str__(self):
        return f"{self.category} > {self.name}"


class ItemAvailability(models.Model):
    item = models.ForeignKey('Item', on_delete=models.RESTRICT, null=False, blank=False)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.RESTRICT, null=False, blank=False)
    quantity_type = models.TextField(choices=QuantityMeasure.choices, null=False, blank=False, default=QuantityMeasure.SingleItem)
    item_size = models.CharField(max_length=32, null=True, blank=True)
    item_color = models.CharField(max_length=32, null=True, blank=True)
    quantity = models.IntegerField(null=False, blank=False)
    date_added = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ['item', 'quantity_type', 'item_size', 'item_color']

    def __str__(self):
        return f"{self.item}: {self.quantity}"

    def get_releases(self):
        return InventoryRelease.objects.filter(item__pk=self.pk)


class InventoryRelease(models.Model):
    item = models.ForeignKey(ItemAvailability, on_delete=models.RESTRICT, null=False, blank=False)
    quantity = models.IntegerField(null=False, blank=False)
    date_released = models.DateTimeField(auto_now_add=True)
    notes = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.quantity > self.item.item.available_quantity:
            raise ValueError("Release quantity > available quantity, please update Inventory first or reduce quantity")
        _availability = ItemAvailability.objects.get(pk=self.item.pk)
        _availability.quantity -= self.quantity
        _availability.save()
        super().save(*args, **kwargs)


class Item(models.Model):
    sub_category = models.ForeignKey(SubCategory, null=False, blank=False, on_delete=models.RESTRICT)
    name = models.CharField(max_length=128, null=False, blank=False)
    code = models.CharField(max_length=64, null=True, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)

    class Meta:
        unique_together = ['sub_category', 'name']

    def __str__(self):
        return f"{self.sub_category.category} > {self.sub_category}: {self.name}"

    @property
    def available_quantity(self):
        _quantity = ItemAvailability.objects.filter(item__pk=self.pk).aggregate(Sum('quantity'))['quantity__sum']
        return _quantity if _quantity else 0

    def get_available_inventory(self):
        return ItemAvailability.objects.filter(item__pk=self.pk)


class Demand(models.Model):
    item = models.ForeignKey(Item, on_delete=models.RESTRICT, null=False, blank=False)
    quantity_type = models.TextField(choices=QuantityMeasure.choices, null=False, blank=False)
    quantity = models.IntegerField(null=False, blank=False)
    date_added = models.DateTimeField(auto_now=True)
    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return f"{self.item}: {self.quantity} {self.quantity_type}"
