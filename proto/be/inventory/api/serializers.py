from rest_framework.serializers import ModelSerializer, SerializerMethodField
from rest_framework.viewsets import ModelViewSet
from ..models import Item, Org, Warehouse
from ..models import Category, SubCategory, Tag
from ..models import ItemAvailability, Demand
from ..models import OrgRegistrationApplication
from ..models import UserProfile
from address.models import Address
from django.contrib.auth.models import User


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        exclude = ['password']


class ProfileSerializer(ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = UserProfile
        fields = '__all__'


class OrgRegistrationApplicationSerializer(ModelSerializer):
    class Meta:
        model = OrgRegistrationApplication
        fields = '__all__'


class PubOrgRegistrationApplicationSerializer(ModelSerializer):
    class Meta:
        model = OrgRegistrationApplication
        exclude = ['review_notes', 'application_status']


class AddressSerializer(ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'


class OrgSerializer(ModelSerializer):
    class Meta:
        model = Org
        fields = '__all__'


class WarehouseSerializer(ModelSerializer):
    class Meta:
        model = Warehouse
        fields = '__all__'


class ItemSerializer(ModelSerializer):
    available_quantity = SerializerMethodField()

    class Meta:
        model = Item
        fields = '__all__'

    def get_available_quantity(self, item):
        return item.available_quantity


class SubCategorySerializer(ModelSerializer):
    class Meta:
        model = SubCategory
        fields = '__all__'


class CategorySerializer(ModelSerializer):
    sub_category = SerializerMethodField()

    class Meta:
        model = Category
        fields = '__all__'

    def get_sub_category(self, category):
        _subs = SubCategory.objects.filter(category=category)
        return SubCategorySerializer(_subs, many=True).data


class TagSerializer(ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class ItemAvailabilitySerializer(ModelSerializer):
    class Meta:
        model = ItemAvailability
        fields = '__all__'


class DemandSerializer(ModelSerializer):
    class Meta:
        model = Demand
        fields = '__all__'

