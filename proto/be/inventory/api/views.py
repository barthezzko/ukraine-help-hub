from .serializers import ItemSerializer, CategorySerializer, SubCategorySerializer, TagSerializer
from .serializers import ItemAvailabilitySerializer, DemandSerializer
from .serializers import OrgSerializer, WarehouseSerializer
from .serializers import AddressSerializer
from .serializers import OrgRegistrationApplicationSerializer
from .serializers import PubOrgRegistrationApplicationSerializer
from .serializers import ProfileSerializer
from rest_framework.viewsets import ModelViewSet, ViewSet, mixins, GenericViewSet
from ..models import Item, Category, SubCategory, Tag, ItemAvailability, Demand, Org, Warehouse
from ..models import OrgRegistrationApplication
from ..models import UserProfile, OrgRegistrationStatus
from address.models import Address
import django_filters
from rest_framework.decorators import action
from rest_framework.response import Response
from django.contrib.auth.models import User
from django.db import transaction


class OrgRegistrationApplicationViewSet(mixins.CreateModelMixin,
                                        mixins.RetrieveModelMixin,
                                        mixins.ListModelMixin,
                                        GenericViewSet):

    def get_serializer(self, *args, **kwargs):
        if self.action == 'create':
            return PubOrgRegistrationApplicationSerializer(*args, **kwargs)
        return OrgRegistrationApplicationSerializer(*args, **kwargs)

    def get_queryset(self):
        return OrgRegistrationApplication.objects.all()

    @action(methods=['post'], detail=True)
    def decline(self, request, pk):
        try:
            application = OrgRegistrationApplication.objects.get(pk=pk)
            if application.application_status == OrgRegistrationStatus.APPROVED:
                raise AttributeError("The application has already been approved")
            if application.application_status == OrgRegistrationStatus.DECLINED:
                raise AttributeError("The application has been already declined")
            application.application_status = OrgRegistrationStatus.DECLINED
            application.review_notes = request.data.get("review_notes")
            application.save()
            return Response({"ok": "application declined"})
        except (AttributeError, ValueError, Exception) as e:
            return Response({"error": "failed to reject application"})

    @action(methods=['post'], detail=True)
    def approve(self, request, pk):
        application = OrgRegistrationApplication.objects.get(pk=pk)
        try:
            with transaction.atomic():
                if application.application_status == OrgRegistrationStatus.APPROVED:
                    raise AttributeError("This application has already been approved")
                if application.application_status == OrgRegistrationStatus.DECLINED:
                    raise AttributeError("This application has already been declined")
                application.application_status = OrgRegistrationStatus.APPROVED
                application.save()

                _org, _org_created = Org.objects.get_or_create(name=application.name,
                                                               web=application.web,
                                                               email=application.email)
                if not _org_created:
                    raise AttributeError(f"Organization ({_org.id}: {_org.name}) already exists")
                _org.phone = application.phone
                _org.save()

                _user, _user_created = User.objects.get_or_create(email=application.admin_email,
                                                                    username=application.admin_email)
                if not _user_created:
                    raise ValueError("User with this email already exists: one user can only be associated with one organization")
                _user.first_name = application.admin_first_name
                _user.last_name = application.admin_last_name
                _user.email = application.admin_email
                _user.password = User.objects.make_random_password()
                _user.save()

                _profile = UserProfile.objects.create(user=_user, org=_org, is_admin=True)

                return Response({"org": OrgSerializer(_org).data,
                                 "profile": ProfileSerializer(_profile).data,
                                 "user_password": _user.password}, status=201)
        except (ValueError, AttributeError) as e:
            return Response({"error": e}, status=400)
        except Exception as e:
            return Response({"error": "server error, please contact administrator"}, status=500)


class AddressViewSet(ModelViewSet):
    serializer_class = AddressSerializer

    def get_queryset(self):
        return Address.objects.all()


class OrgViewSet(ModelViewSet):
    serializer_class = OrgSerializer

    def get_queryset(self):
        return Org.objects.all()


class WarehouseViewSet(ModelViewSet):
    serializer_class = WarehouseSerializer

    def get_queryset(self):
        return Warehouse.objects.all()


class ItemViewSet(ModelViewSet):
    serializer_class = ItemSerializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['name']

    def filter_queryset(self, queryset):
        for f in self.filter_backends:
            queryset = f().filter_queryset(self.request, queryset=queryset, view=self)
        return queryset

    def get_queryset(self):
        return Item.objects.all()


class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer

    def get_queryset(self):
        return Category.objects.all()


class SubCategoryViewSet(ModelViewSet):
    serializer_class = SubCategorySerializer

    def get_queryset(self):
        return SubCategory.objects.all()


class TagViewSet(ModelViewSet):
    serializer_class = TagSerializer

    def get_queryset(self):
        return Tag.objects.all()


class ItemAvailabilityViewSet(ModelViewSet):
    serializer_class = ItemAvailabilitySerializer

    def get_queryset(self):
        return ItemAvailability.objects.all()
"""
    def filter_queryset(self, queryset):
        for f in self.filter_backends:
            queryset = f().filter_queryset(self.request, queryset=queryset, view=self)
        return queryset
"""

class DemandViewSet(ModelViewSet):
    serializer_class = DemandSerializer

    def get_queryset(self):
        return Demand.objects.all()
