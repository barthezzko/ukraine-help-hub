from django.test import TestCase
from inventory.models import Org, Warehouse, Item, Category, SubCategory, ItemAvailability, InventoryRelease, Tag, \
    OrgRegistrationApplication
from inventory.models import QuantityMeasure
from inventory.models import OrgRegistrationStatus
from faker import Faker
import requests


class BaseDataTest(TestCase):

    def setUp(self) -> None:
        org_one = Org.objects.create(name="Org One", active=True, phone="+16462760970")
        org_two = Org.objects.create(name="Org Two", active=True, email="abolotnov@gmail.com")

        warehouse_1 = Warehouse.objects.create(name="Kyev", org=org_one)
        warehouse_2 = Warehouse.objects.create(name="Lviv", org=org_two)

        food_category = Category.objects.create(name="Food & Drinks")
        cloths_category = Category.objects.create(name="Male Cloths")
        medicine_category = Category.objects.create(name="Medicine")

        food_sub = SubCategory.objects.create(category=food_category, name="Soft Drinks")
        snacks_sub = SubCategory.objects.create(category=food_category, name="Snacks")
        pasta_sub = SubCategory.objects.create(category=food_category, name="Pasta and Noodles")

        SubCategory.objects.create(category=cloths_category, name="Shoes")
        SubCategory.objects.create(category=cloths_category, name="Jackets & Coats")
        SubCategory.objects.create(category=cloths_category, name="Underwear")

        SubCategory.objects.create(category=medicine_category, name="First Aid Kits")
        SubCategory.objects.create(category=medicine_category, name="Pain Relief")
        SubCategory.objects.create(category=medicine_category, name="Skin Care")

        item_water = Item.objects.create(sub_category=food_sub, name="Water")
        item_coke = Item.objects.create(sub_category=food_sub, name="Coka Cola")

        availability_1l_water = ItemAvailability.objects.create(item=item_water,
                                                                warehouse=warehouse_1,
                                                                quantity=100,
                                                                quantity_type=QuantityMeasure.Bottle,
                                                                item_size="2L")
        for i in range(5):
            InventoryRelease.objects.create(item=availability_1l_water, quantity=5)

    def test_all_created(self):
        self.assertEqual(Category.objects.count(), 3)
        self.assertEqual(SubCategory.objects.count(), 9)
        self.assertEqual(Item.objects.count(), 2)
        self.assertEqual(Item.objects.get(name="Water",
                                          itemavailability__quantity_type=QuantityMeasure.Bottle,
                                          itemavailability__item_size="2L").available_quantity, 75)

    def test_over_draft(self):
        with self.assertRaises(ValueError):
            availability = Item.objects.filter(name="Water",
                                               itemavailability__item_size="2L")[0].get_available_inventory()
            InventoryRelease.objects.create(item=availability, quantity=200)

    def test_inventory_item_with_tags(self):
        item = Item()
        item.name = "Coca Cola"
        item.sub_category = SubCategory.objects.get(name="Soft Drinks")
        item.item_size = "2L"
        item.item_color = "Black"
        tag_gas, created = Tag.objects.get_or_create(name="gas")
        tag_sweet, created = Tag.objects.get_or_create(name="sweet")
        item.save()
        item.tags.add(tag_gas, tag_sweet)
        self.assertEquals(item.tags.count(), 2)

    def test_tag_saves_lower(self):
        tag = Tag.objects.create(name="BIG")
        self.assertEqual(tag.name, "big")

    def test_application_creation(self):
        fake = Faker()
        application_data = {
            'name': fake.name(),
            'web': fake.url(),
            'phone': '+12223334455',
            'email': fake.email(),
            'admin_first_name': fake.first_name(),
            'admin_last_name': fake.last_name(),
            'admin_email': fake.email(),
            'admin_phone': '+12236078800',
            'notes': fake.text()
        }
        outcome = requests.post('http://localhost:8000/api/org_application/', data=application_data)
        self.assertEqual(outcome.status_code, 201)

    def test_declines(self):
        # get some random application to decline
        org = OrgRegistrationApplication.objects.filter(application_status=OrgRegistrationStatus.PENDING_APPROVAL.value).order_by('?')[:1]
        outcome = requests.post(f"http://localhost:8000/api/org_application/{org.pk}/decline/")
        self.assertEqual(outcome.status_code, 200)

